<?php

namespace Tests\Feature;


use App\Models\UserRequest;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class SubmitFunctionalityTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Session::start();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSubmit()
    {
        $user = \App\Models\User::factory()->create();

        $data = [
            'api_url' => 'https://public.api.ea2.openprocurement.net/api/2.3/auctions/86faded9e2ac4a43845157f762a983b2',
            'send_result' => 'on',
            '_token' => csrf_token(),
        ];
        $response = $this->actingAs($user)->post('/submit', $data);

        $response->assertSessionHas('success');
        $response->assertStatus(302);
    }

    public function testProcessUsersRequests()
    {
        $request = UserRequest::factory([
            'api_url' => 'https://public.api.ea2.openprocurement.net/api/2.3/auctions/86faded9e2ac4a43845157f762a983b2'
        ])->create();

        Artisan::call('processRequests');

        $processed = UserRequest::find($request->id);

        $this->assertEquals(1, $processed->is_processed);
        $this->assertEquals(200, $processed->status);
    }
}
