<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubmitRequest;
use App\Models\UserRequest;
use Illuminate\Support\Facades\Auth;

class SubmitController extends Controller
{

    public function create(SubmitRequest $request)
    {
        $data = $request->validated();
        $existed = UserRequest::where('user_id', Auth::id())->where('api_url', $data['api_url'])->first();
        if($existed) {
            return redirect()->back()->withErrors(['You are trying to add an already existing request'])->withInput($request->input());
        }

        UserRequest::create([
            'user_id' => Auth::id(),
            'api_url' => $data['api_url'],
            'send_result' => ($request->input('send_result') == 'on') ? 1 : 0,
        ]);

        return redirect()->back()->with('success', 'Your request will be processed shortly');
    }
}
