<?php

namespace App\Jobs;

use App\Mail\SendNotification;
use App\Models\UserRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

class SendApiRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var UserRequest
     */
    private $request;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = Http::acceptJson()->get($this->request->api_url);

        if ($response->failed()) {
            $this->request->processedWithError($response->status());
            if ($this->request->send_result) {
                $user = $this->request->user;
                $data = 'Your request failed with status: ' . $response->status();
                Mail::to($user->email)->queue(new SendNotification($data, $this->request->api_url, $user));
            }
        } else {
            $this->request->processed($response->status());
            if ($this->request->send_result) {
                $user = $this->request->user;
                $data = json_decode($response->getBody(), true)['data'] ?? [];
                Mail::to($user->email)->queue(new SendNotification($data, $this->request->api_url, $user));
            }
        }
    }
}
