<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRequest extends Model
{
    use HasFactory;

    protected $guarded = [];
    public $timestamps = TRUE;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function processed(int $status)
    {
        $this->is_processed = 1;
        $this->status = $status;
        $this->save();
    }

    public function processedWithError(int $status) {
        $this->is_failed = 1;
        $this->processed($status);
    }
}
