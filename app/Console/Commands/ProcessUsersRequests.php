<?php

namespace App\Console\Commands;

use App\Jobs\SendApiRequest;
use App\Models\UserRequest;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ProcessUsersRequests extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'processRequests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch all user requests and run them';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $requests = UserRequest::where('is_processed', '<>', '1')->get();
        $requests->each(function($request) {
            $this->dispatch(new SendApiRequest($request));
        });
    }
}
