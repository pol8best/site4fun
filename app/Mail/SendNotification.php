<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $body;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($body, $apiURL, $user)
    {
        $this->user = $user;
        $this->url  = $apiURL;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.request-done')
                    ->subject('Request done');
    }
}
