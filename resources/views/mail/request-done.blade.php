@component('mail::message')

    Good afternoon {{ $user->user_name }}!<br>
    Your request has been successfully completed.<br>
    Query Result:<br>
    "{{ $url }}"
    <br><br>
    {{ print_r($body) }}
    <br><br>
    Best regards, {{ config('app.name') }}

@endcomponent
