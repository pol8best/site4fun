@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" role="alert">{{ $error }}</div>
                        @endforeach
                    @endif
                    <div class="d-flex flex-column">
                        <form action="{{ route('submit') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputPassword1">Request URL</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Url" name="api_url"
                                       value="{{ old('api_url') }}">
                            </div>
                            <div class="form-check pb-2">
                                <input id="exampleCheck1" type="checkbox" class="form-check-input" name="send_result"
                                       @if(old('send_result')) checked @endif>
                                <label for="exampleCheck1" class="form-check-label">Send me the request result</label>
                            </div>
                            <button type="submit" class="btn btn-primary col-md-4 offset-md-4">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Auth::user()->requests->count())
    <div class="row justify-content-center pt-4">
        <div class="col-md-8">
            @include('components.user-request-table', ['requests' => Auth::user()->requests])
        </div>
    </div>
    @endif
</div>
@endsection
