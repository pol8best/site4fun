<div class="card">
    <table class="table user-requests" style="max-width: 100%;">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">URL</th>
            <th scope="col">Status</th>
            <th scope="col">Added at</th>
        </tr>
        </thead>
        <tbody>
        @foreach($requests as $key => $request)
            <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>
                    <div class="text-truncate" style="max-width: 500px;"
                         title="{{ $request->api_url }}">{{ $request->api_url }}</div>
                </td>
                <td>{{ $request->status }}</td>
                <td><small class="text-muted">{{ $request->created_at }}</small></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
